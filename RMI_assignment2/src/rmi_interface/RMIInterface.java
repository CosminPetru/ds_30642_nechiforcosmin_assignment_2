package rmi_interface;

import commun_classes.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {
    double computeTax(Car c) throws RemoteException;
    double computeSellingPrice(Car c) throws RemoteException;
}
