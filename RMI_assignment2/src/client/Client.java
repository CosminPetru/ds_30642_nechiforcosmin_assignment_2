package client;

import client.ui.Application;
import commun_classes.Car;
import rmi_interface.RMIInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;


public class Client{

    private static RMIInterface look_up;

    public static void main(String[] args)
            throws MalformedURLException, RemoteException, NotBoundException {

        look_up = (RMIInterface) Naming.lookup("//localhost/MyServer");

        Application app = new Application();
        app.getFrame1().setVisible(true);
        app.getTaxButton().addActionListener(actionEvent -> {
            int year = Integer.parseInt(app.getYearTextField().getText());
            int price = Integer.parseInt(app.getPriceTextField().getText());
            int engine_size = Integer.parseInt(app.getEngineSizeTextField().getText());
            try {
                double response = look_up.computeTax(new Car(year, engine_size, price));
                JOptionPane.showMessageDialog(null, "Tax: " + response);
            } catch (RemoteException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Remote exception thrown");
            }
        });

        app.getSellingPriceButton().addActionListener(actionEvent -> {
            int year = Integer.parseInt(app.getYearTextField().getText());
            int price = Integer.parseInt(app.getPriceTextField().getText());
            int engine_size = Integer.parseInt(app.getEngineSizeTextField().getText());
            try {
                double response = look_up.computeSellingPrice(new Car(year, engine_size, price));
                JOptionPane.showMessageDialog(null, "Selling price: " + response);
            } catch (RemoteException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Remote exception thrown");
            }
        });
    }

}