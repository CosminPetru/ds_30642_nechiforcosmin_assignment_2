/*
 * Created by JFormDesigner on Sat Nov 03 14:47:19 EET 2018
 */

package client.ui;

import java.awt.*;
import javax.swing.*;
import net.miginfocom.swing.*;

/**
 * @author unknown
 */
public class Application extends JPanel {
    public Application() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Cosmin-Petru Nechifor
        frame1 = new JFrame();
        label2 = new JLabel();
        label1 = new JLabel();
        yearTextField = new JTextField();
        label3 = new JLabel();
        engineSizeTextField = new JTextField();
        label4 = new JLabel();
        priceTextField = new JTextField();
        label5 = new JLabel();
        taxButton = new JButton();
        sellingPriceButton = new JButton();

        //======== frame1 ========
        {
            Container frame1ContentPane = frame1.getContentPane();
            frame1ContentPane.setLayout(null);

            //---- label2 ----
            label2.setText("Car information:");
            frame1ContentPane.add(label2);
            label2.setBounds(20, 35, 115, 25);

            //---- label1 ----
            label1.setText("Assignment 2");
            frame1ContentPane.add(label1);
            label1.setBounds(165, 10, 100, label1.getPreferredSize().height);
            frame1ContentPane.add(yearTextField);
            yearTextField.setBounds(120, 65, 155, yearTextField.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Year:");
            frame1ContentPane.add(label3);
            label3.setBounds(20, 65, label3.getPreferredSize().width, 25);
            frame1ContentPane.add(engineSizeTextField);
            engineSizeTextField.setBounds(120, 100, 155, engineSizeTextField.getPreferredSize().height);

            //---- label4 ----
            label4.setText("Engine SIze:");
            frame1ContentPane.add(label4);
            label4.setBounds(new Rectangle(new Point(20, 105), label4.getPreferredSize()));
            frame1ContentPane.add(priceTextField);
            priceTextField.setBounds(120, 140, 155, priceTextField.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Price:");
            frame1ContentPane.add(label5);
            label5.setBounds(new Rectangle(new Point(20, 145), label5.getPreferredSize()));

            //---- taxButton ----
            taxButton.setText("Tax");
            frame1ContentPane.add(taxButton);
            taxButton.setBounds(new Rectangle(new Point(155, 190), taxButton.getPreferredSize()));

            //---- sellingPriceButton ----
            sellingPriceButton.setText("Selling price");
            frame1ContentPane.add(sellingPriceButton);
            sellingPriceButton.setBounds(new Rectangle(new Point(25, 190), sellingPriceButton.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < frame1ContentPane.getComponentCount(); i++) {
                    Rectangle bounds = frame1ContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = frame1ContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                frame1ContentPane.setMinimumSize(preferredSize);
                frame1ContentPane.setPreferredSize(preferredSize);
            }
            frame1.pack();
            frame1.setLocationRelativeTo(frame1.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Cosmin-Petru Nechifor
    private JFrame frame1;
    private JLabel label2;
    private JLabel label1;
    private JTextField yearTextField;
    private JLabel label3;
    private JTextField engineSizeTextField;
    private JLabel label4;
    private JTextField priceTextField;
    private JLabel label5;
    private JButton taxButton;
    private JButton sellingPriceButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JButton getSellingPriceButton() {
        return sellingPriceButton;
    }

    public void setSellingPriceButton(JButton sellingPriceButton) {
        this.sellingPriceButton = sellingPriceButton;
    }

    public JButton getTaxButton() {
        return taxButton;
    }

    public void setTaxButton(JButton taxButton) {
        this.taxButton = taxButton;
    }

    public JTextField getYearTextField() {
        return yearTextField;
    }

    public void setYearTextField(JTextField yearTextField) {
        this.yearTextField = yearTextField;
    }

    public JTextField getEngineSizeTextField() {
        return engineSizeTextField;
    }

    public void setEngineSizeTextField(JTextField engineSizeTextField) {
        this.engineSizeTextField = engineSizeTextField;
    }

    public JTextField getPriceTextField() {
        return priceTextField;
    }

    public void setPriceTextField(JTextField priceTextField) {
        this.priceTextField = priceTextField;
    }

    public JFrame getFrame1() {
        return frame1;
    }

    public void setFrame1(JFrame frame1) {
        this.frame1 = frame1;
    }
}
