package server;

import commun_classes.Car;
import rmi_interface.RMIInterface;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class Server extends UnicastRemoteObject implements RMIInterface{

    private static final long serialVersionUID = 1L;

    protected Server() throws RemoteException {
        super();
    }

    public static void main(String[] args){
        try {
            System.setProperty("java.rmi.server.hostname","localhost");
            Naming.rebind("//localhost/MyServer", new Server());
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public double computeTax(Car c) throws RemoteException {
        // Dummy formula
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }

    @Override
    public double computeSellingPrice(Car c) throws RemoteException {
        System.out.println("Compute selling price was called");
        double purchasingPrice = c.getPurchasingPrice();
        int year = c.getYear();
        if (2018 - year < 7)
            return purchasingPrice - ((purchasingPrice / 7) * (2018 - year));
        return 0;
    }
}

